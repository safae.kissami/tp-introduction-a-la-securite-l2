#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "bignum.h"
#include "list.h"


unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = b;
  if(a < b)
    return gcd(b,a);

  while(r != 0){
    old_r = r;
    r = a % b;
    a = b;
    b = r;
  }

  return old_r;
}
unsigned long int f(unsigned long int x){
  return x*x+1;
}

int test(unsigned long int a, unsigned long int b, unsigned long int n){
  return a>b ? gcd(a-b,n)!=1 : gcd(b-a,n)!=1;
}

int hasCycle(list_t l, unsigned long int n){
  unsigned long int x,y;
  if(!list_is_empty(l)){
    x = *(unsigned long int *)(list_top(l));
    l = list_tail(l);
    while(!list_is_empty(l)){
      y = *(unsigned long int *)(list_top(l));
      l = list_tail(l);
      if(test(x,y,n)){
        printf("Detection OK\n");
        return 1;
      }
    }
  }
  printf("Pas de detection\n");
  return 0;
}
list_t roPollard(unsigned long int n){
  list_t l = list_empty();
  srand(time(NULL));
  unsigned long int * x = malloc(sizeof(unsigned long int));
  *x = rand()%n;
  l = list_push(l, x);
  printf("%lu\n", *x);
  while(!hasCycle(l, n)){
    unsigned long int * y = malloc(sizeof(unsigned long int));
    *y = f(*((unsigned long int *)list_top(l)))%n;
    printf("%lu\n", *y);
    l = list_push(l, y);
  }
  return l;
}
unsigned long int floyd(unsigned long int n)
{
	unsigned long int x=2;
	unsigned long int y=2;
	unsigned long int d=1;
	while(d==1)
	{
		x=f(x)%n;
		y=f(f(y))%n;
		d=gcd(x-y,n);
	}
	
	return d;
}
int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17 * 113;
  unsigned long int n2 = 239 * 431;
  unsigned long int n3 = 3469 * 4363;
  unsigned long int n4 = 15241 * 18119;
  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;

  bignum_t n7, n8;
  
  floyd(n5);
  printf("floyed = %lu\n", floyd(n1));
  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));

	
  
  printf("PGCD(42,24) = %lu\n", gcd(42,24));
  printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));
  
  return 0;
}
