#include <stdlib.h>
#include "list.h"

#define ARRAY_CAPACITY 1 << 16

struct hash_tbl {
  size_t (*h)(void*);
  int (*eq)(void*, void*);
  void (*free)(void*);
  unsigned long int size;
  size_t capacity; // array length
  list_t* array;
};

typedef struct hash_tbl* hash_tbl;

hash_tbl htbl_empty(size_t (*h)(void*),int (*eq)(void*, void*),void (*free)(void*)){
  hash_tbl hash = malloc(sizeof(struct hash_tbl));
  hash->h = h;
  hash->eq = eq;
  hash->free = free;
  hash->size = 0;
  hash->capacity = ARRAY_CAPACITY;
  hash->array = malloc(sizeof(list_t) * hash->capacity);
  return hash;
}

int htbl_add(hash_tbl htbl, void* x){
   if(list_in(htbl->array[htbl->h(x)], x, htbl->eq) == NULL)
  {
    htbl->size++ ;
    htbl->array[htbl->h(x)] = list_push(htbl->array[htbl->h(x)], x);
    return 1;
  }
  else
    return 0;
}


void* htbl_in(hash_tbl htbl, void* x){
    if(list_in(htbl->array[htbl->h(x)], x, htbl->eq) != NULL)
    return x;
  return NULL;



void htbl_destroy(hash_tbl htbl){
 
}  
